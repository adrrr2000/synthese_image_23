#include <vector>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <glad/glad.h>
#include <cassert>
#include <glimac/FilePath.hpp>
#include <glimac/Program.hpp>
#include <glimac/glm.hpp>

struct Vertex2DColor {
    glm::vec2 position;
    glm::vec3 color;
    Vertex2DColor()
    {
    }
    Vertex2DColor(glm::vec2 pos, glm::vec3 col)
    {
        position = pos;
        color    = col;
    }
};

int window_width  = 1280;
int window_height = 720;

static void key_callback(GLFWwindow* /*window*/, int /*key*/, int /*scancode*/, int /*action*/, int /*mods*/)
{
}

static void mouse_button_callback(GLFWwindow* /*window*/, int /*button*/, int /*action*/, int /*mods*/)
{
}

static void scroll_callback(GLFWwindow* /*window*/, double /*xoffset*/, double /*yoffset*/)
{
}

static void cursor_position_callback(GLFWwindow* /*window*/, double /*xpos*/, double /*ypos*/)
{
}

static void size_callback(GLFWwindow* /*window*/, int width, int height)
{
    window_width  = width;
    window_height = height;
}

int main([[maybe_unused]] int argc, char* argv[])
{
    /* Initialize the library */
    if (!glfwInit()) {
        return -1;
    }

    /* Create a window and its OpenGL context */
#ifdef __APPLE__
    /* We need to explicitly ask for a 3.3 context on Mac */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
    GLFWwindow* window = glfwCreateWindow(window_width, window_height, "TP3", nullptr, nullptr);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    /* Intialize glad (loads the OpenGL functions) */
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
        return -1;
    }

    /* Hook input callbacks */
    glfwSetKeyCallback(window, &key_callback);
    glfwSetMouseButtonCallback(window, &mouse_button_callback);
    glfwSetScrollCallback(window, &scroll_callback);
    glfwSetCursorPosCallback(window, &cursor_position_callback);
    glfwSetWindowSizeCallback(window, &size_callback);

    glimac::FilePath applicationPath(argv[0]);
    glimac::Program  program = glimac::loadProgram(applicationPath.dirPath() + "TP3/shaders/triangle.vs.glsl",
                                                   applicationPath.dirPath() + "TP3/shaders/triangle.fs.glsl");

    program.use();

    // CREATION DU VBO
    GLuint vbo;
    glGenBuffers(1, &vbo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo); // binder le buffer

    const size_t               N = 30; // nombre de triangle
    std::vector<Vertex2DColor> vertices;
    float                      teta;
    for (size_t i = 0; i < N; i++) {
        teta = (i * 2 * glm::pi<float>()) / N;
        vertices.push_back(Vertex2DColor(glm::vec2(0, 0), glm::vec3(1, 1, 1)));
        vertices.push_back(Vertex2DColor(glm::vec2(glm::cos(teta), glm::sin(teta)), glm::vec3(1, 1, 1)));
        vertices.push_back(Vertex2DColor(glm::vec2(glm::cos((2 * glm::pi<float>()) / N + teta), glm::sin((2 * glm::pi<float>()) / N + teta)), glm::vec3(1, 1, 1)));
    };

    // Vertex2DColor vertices2[] = {
    //     Vertex2DColor(glm::vec2(-0.5, -0.5), glm::vec3(0, 0, 0)),
    //     Vertex2DColor(glm::vec2(0.5, -0.5), glm::vec3(0, 0, 0)),
    //     Vertex2DColor(glm::vec2(-0.5, 0.5), glm::vec3(0, 0, 0)), Vertex2DColor(glm::vec2(-0.5, 0.5), glm::vec3(0, 0, 0)),
    //     Vertex2DColor(glm::vec2(0.5, -0.5), glm::vec3(0, 0, 0)),
    //     Vertex2DColor(glm::vec2(0.5, 0.5), glm::vec3(0, 0, 0))};

    glBufferData(GL_ARRAY_BUFFER, 3 * N * sizeof(Vertex2DColor), vertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0); // debinder le vbo

    // CREATION DU VAO
    GLuint vao;
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao); // binder les vao

    const GLuint VERTEX_ATTR_POSITION = 3;
    const GLuint VERTEX_ATTR_COLOR    = 8;
    glEnableVertexAttribArray(VERTEX_ATTR_POSITION); // attribut des vertex, avec 3 pour la position
    glEnableVertexAttribArray(VERTEX_ATTR_COLOR);    // attribut des vertex, avec 8 pour la couleur

    glBindBuffer(GL_ARRAY_BUFFER, vbo); // binder le buffer a nouveau

    glVertexAttribPointer(VERTEX_ATTR_POSITION, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const GLvoid*)offsetof(Vertex2DColor, position)); // specification des attributs de vertex

    glVertexAttribPointer(VERTEX_ATTR_COLOR, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex2DColor), (const GLvoid*)offsetof(Vertex2DColor, color)); // specification des attributs de vertex

    glBindBuffer(GL_ARRAY_BUFFER, 0); // debinder le vbo

    glBindVertexArray(0); // debinder le vao

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT);

        glBindVertexArray(vao); // binder le vao

        glDrawArrays(GL_TRIANGLES, 0, 3 * N); // on dessine le triangle

        glBindVertexArray(0); // debinder le vao

        /* Swap front and back buffers */
        glfwSwapBuffers(window);
        /* Poll for and process events */
        glfwPollEvents();
    }

    // liberation des ressources
    glDeleteBuffers(1, &vbo);
    glDeleteVertexArrays(1, &vao);

    glfwTerminate();

    return 0;
}